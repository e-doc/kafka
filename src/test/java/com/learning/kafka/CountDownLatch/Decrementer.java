package com.learning.kafka.CountDownLatch;

import java.util.concurrent.CountDownLatch;

/**
 * Created by gyliarde on 01/05/19.
 */
public class Decrementer implements Runnable {

    CountDownLatch latch = null;

    public Decrementer(CountDownLatch latch) {
        this.latch = latch;
    }

    public void run() {

        try {
            Thread.sleep(1000);
            this.latch.countDown();
            System.out.println("latchCount : " +   this.latch.getCount());

            Thread.sleep(1000);
            this.latch.countDown();
            System.out.println("latchCount : " +   this.latch.getCount());

            Thread.sleep(1000);
            this.latch.countDown();
            System.out.println("latchCount : " +   this.latch.getCount());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
