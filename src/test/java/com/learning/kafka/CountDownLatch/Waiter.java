package com.learning.kafka.CountDownLatch;

import java.util.concurrent.CountDownLatch;

/**
 * Created by gyliarde on 01/05/19.
 */
public class Waiter implements Runnable{

    CountDownLatch latch = null;

    public Waiter(CountDownLatch latch) {
        this.latch = latch;
    }

    public void run() {
        try {
            latch.await();
            System.out.println("Latch count waiter " + this.latch.getCount() );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Waiter Released");
    }
}
