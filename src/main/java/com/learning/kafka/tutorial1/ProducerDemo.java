package com.learning.kafka.tutorial1;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

/**
 * Created by gyliarde on 29/04/19.
 */
public class ProducerDemo {

    public static void main(String[] args) {
        String bootstrapServers= "localhost:9092";

        //create Producer Properties

        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());

        //create the producer
        KafkaProducer<String,String> producer =new KafkaProducer<String, String>(properties);

        //Create a producer record
        ProducerRecord<String,String> record = new ProducerRecord<String, String>("first_topic","hello_world mano");

        //send data
        producer.send(record);

        //flush data
        producer.flush();

        //flush abd close producer
        producer.close();
    }
}
